/**
 * @author Bhupesh Shrestha
 * Date: 8/30/2020
 * Lab 4.2-Primitives
 */

public class Primitives {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     
		int a = 20;
		float b = 2.34f;
		char c = 'e';
		double d = 2.34;
		boolean bool = true;
		String name = "Java";
		
		//Printing out the values of primitives and string
	    System.out.println("The value of integer a is: " + a);
	    System.out.println("The value of float b is: " + b);
	    System.out.println("The value of char c is: " + c);
	    System.out.println("The value of double d is: " + d);
	    System.out.println("The value of boolean bool is: " + bool);
	    System.out.println("The value of string name is: " + name);
	    
	}

}
