/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/03/2020
 * Lab 5.2-String Manipulation
 *
 */
public class FixString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String a = "Jovo is";
		String b = " a great Langu";
		String c = b.concat("age!");
		char ch = 'a';
		
     	String d = a.substring(0, 1) + ch + a.substring(2,3) + ch + a.substring(4);
	    String e = d + c;	
		
		System.out.println(e.toUpperCase());
		
	}

}
