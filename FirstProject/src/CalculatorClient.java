/**
 * 
 * @author Bhupesh Shrestha
 * Date: 8/30/2020
 * Lab 4.4 - Write the client (Calculator)
 * 
 */

public class CalculatorClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
         Calculator cal = new Calculator();
         
         int add = cal.addNumber(2,3); //adding two numbers
         System.out.println(add);
         
         int sub = cal.subtractNumber(3,2); //subtracting two numbers
         System.out.println(sub); 
         
         int multi = cal.multiplyNumber(2,3); //multiplying two numbers
         System.out.println(multi);
         
         int div = cal.divideNumber(4,2); //dividing two numbers
         System.out.println(div);
         
         int integer = cal.integerDivision(15, 5); // dividing two integers
         System.out.println(integer);
         
         int len = cal.stringLen("length"); // shows the length of the String    
         System.out.println(len);
         
         Boolean bool = cal.isTrue(7, 5); // checking the boolean value 
         System.out.println(bool);
	}

}
