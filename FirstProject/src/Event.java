/**
 * 
 * @author Bhupesh Shrestha
 * Date: 8/30/2020
 * Lab 4.3 - An Event Class
 * 
 */
public class Event {

	//method that prints a string value without returning anything
	void method1() {
		System.out.println("Hello");
	}
	
	//method that takes one integer value as a parameter without returning anything
	void method2(int x) {
		System.out.println(x);
	}

	//method that takes two integer value and returns the sum of them
	public int method3(int x, int y) {
		return x + y;
	}
	
}
