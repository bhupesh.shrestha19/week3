/**
 * 
 * @author Bhupesh Shrestha
 * Date: 8/30/2020
 * Lab 4.4 - A Simple Calculator
 * 
 */

public class Calculator {
	
    // method to add two numbers and return the value
	int addNumber(int x, int y) {
		return x + y;
	}
	
	// method to add subtract two numbers and return the value
    int subtractNumber(int x, int y) {
		return x - y;
	}

    // method to multiply two numbers and return the value 
    int multiplyNumber(int x, int y) {
	    return x * y;
    }
    
    // method to divide two numbers and return the value
    int divideNumber(int x, int y) {
	    return x / y;
    }
    
    // method to add two Integer numbers and return the value
    int integerDivision(int x, int y) {
    	return x / y;
    }
    
    int stringLen(String str ) {
     	return str.length();
   }
    
    // method to check if one number is greater than other and return the boolean value
    Boolean isTrue(int x, int y) {
    	if(x>y) {
    		return true;
    	}
    	
    	else {
    		return false;
    	}
    }

}
