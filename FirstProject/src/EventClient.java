/**
 * 
 * @author Bhupesh Shrestha
 * Date: 8/30/2020
 * Lab 4.3 - Write the client
 * 
 */

public class EventClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Event evt = new Event(); //creating object from class Event
		evt.method1();
        evt.method2(3);
        
        int sum = evt.method3(1,2);
        System.out.println(sum);
        
	}

}
