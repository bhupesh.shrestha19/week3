/**
 * 
 * @author Bhupesh Shrestha
 * Date: 09/03/2020
 * Lab 5.1: Object References
 */

public class IdentityTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      
		// creating new objects
		Event evt1 = new Event();
		Event evt2 = new Event();
		
		Event evt3 = evt2;
		
		Boolean isTrue = (evt1 ==evt2);
		Boolean isTrueRef = (evt2 == evt3);
		
		System.out.println(isTrue);
		System.out.println(isTrueRef);
		
		String s1 = "abc";
		String s2 = "abc";
		
		Boolean isTrueString = (s1 == s2);
		System.out.println(isTrueString);
		
		String s3 = new String("abc");
		
		Boolean isTrueNew = (s1 == s3);
		s1.equals(s3);
		System.out.println(isTrueNew);
	}

}
